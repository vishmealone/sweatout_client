package com.example.vishnu.sweatout;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.vishnu.sweatout.Common.ApiClient;
import com.example.vishnu.sweatout.Common.Common;
import com.example.vishnu.sweatout.Fragments.AccountFragment;
import com.example.vishnu.sweatout.Fragments.AppLoginFragment;
import com.example.vishnu.sweatout.Fragments.AppStartFragment;
import com.example.vishnu.sweatout.Fragments.HomeFragment;
import com.example.vishnu.sweatout.Fragments.HostGameFragment;
import com.example.vishnu.sweatout.Fragments.JoinGameFragment;
import com.example.vishnu.sweatout.Fragments.LoadingFragment;
import com.example.vishnu.sweatout.Fragments.OffersFragment;
import com.example.vishnu.sweatout.Fragments.OnBoardingFragment;
import com.example.vishnu.sweatout.Fragments.RegisterFragment;
import com.example.vishnu.sweatout.Fragments.SlotsFragment;
import com.example.vishnu.sweatout.Fragments.SummaryFragment;
import com.example.vishnu.sweatout.Fragments.TransactionsFragment;
import com.example.vishnu.sweatout.Fragments.ViewFragment;
import com.example.vishnu.sweatout.Interfaces.HttpClient;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Bookings;
import com.example.vishnu.sweatout.Models.Customer;
import com.example.vishnu.sweatout.Models.OTPResponse;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.Models.Turfs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MainInterface {
    protected Context context;

    //FIREBASE INSTANCES
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private Calendar horizontalCalenderPreDate;
    private static String TAG = "sweatout";
    private Date selectedBookingDate;
    private String nextFragment;

    private Offers userAppliedOffer;
    BottomNavigationView navigation;
    private String userLocation;
    private Turfs selectedTurf;

    private List<Turfs> locationTurfs = new ArrayList<>();
    private List<Turfs> parentLocationData = new ArrayList<>();
    private List<Turfs> allRegisteredTurfs = new ArrayList<>();
    private List<Turfs> locationDateTimeTurfList = new ArrayList<>();
    private List<Bookings> bookedList = new ArrayList<>();

    private List<Bookings> userBookings = new ArrayList<>();
    private List<String> slideImages = new ArrayList<>();
    private List<Offers> allOffers = new ArrayList<>();
    private Set<String> locationList = new HashSet<>();
    private Set<Map<String, String>> userSelectedSlots = new HashSet<>();

    private Offers[] appliedOfferDt = {new Offers()};
    private List<Map<String, String>> selectedTurfSlots = new ArrayList<>();

    private Date turfFilterDate;
    private String userContact;

    private HttpClient smsclient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FOR FULLSCREEN VIEW
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //FIREBASE INSTANCE DECLARATIONS
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        gettingAllTurfs();
        setContentView(R.layout.activity_main);

        // Call the function callInstamojo to start payment here
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setVisibility(View.INVISIBLE);

        //SLIDER IMAGES - NTC
        slideImages.add("https://firebasestorage.googleapis.com/v0/b/sweatout-cf47c.appspot.com/o/banner3.png?alt=media&token=c983a939-ef1c-49f7-bf84-39b5b460988f");
        slideImages.add("https://firebasestorage.googleapis.com/v0/b/sweatout-cf47c.appspot.com/o/banner3.png?alt=media&token=c983a939-ef1c-49f7-bf84-39b5b460988f");
        slideImages.add("https://firebasestorage.googleapis.com/v0/b/sweatout-cf47c.appspot.com/o/banner3.png?alt=media&token=c983a939-ef1c-49f7-bf84-39b5b460988f");

        gettingAllTurfs();

        //CURRENT LOCATION FETCHING
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        } else {
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            try {
                getLocation(location.getLatitude(), location.getLongitude());
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT);
            }
        }

        Date todaysDate = new Date();

        selectedBookingDate = settingDate(todaysDate);

//        getDateFromString(d.toString());

        //GETTING TODAYS DATE
//        Calendar startDate = Calendar.getInstance();
//        startDate.add(Calendar.MONTH, 0);
//
//        int date = startDate.get(Calendar.DATE);
//        int month = startDate.get(Calendar.MONTH) + 1;
//        selectedBookingDate = startDate.get(Calendar.YEAR) + "/" + month + "/" + date;

        //NETWORK FAILURE NOTIFICATION
        Common cmF = new Common();
        cmF.setView(findViewById(android.R.id.content));
        cmF.setContext(context);
        this.registerReceiver(cmF.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        //INITIAL LOADING PAGE
        loadAllOffers();
        loadAllRegisteredTurfs();
        openLoadingPage();

    }

    //BOTTOM NAVIGATION
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment(getSupportFragmentManager());
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_bookings:
                    fragment = new HostGameFragment(getSupportFragmentManager());
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_search:
                    fragment = new JoinGameFragment(getSupportFragmentManager());
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_profile:
                    fragment = new AccountFragment(getSupportFragmentManager());
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    //CALL FOR PAYMENT IN INSTAMOJO
    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", "1");
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    private InstapayListener listener;

    //INSTAMOJO LISTENER
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                registerBookings();
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplication());

                builder.setMessage("Your Booking has been confirmed...Start warming up!")
                        .setTitle("SUCCESS");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        openHomeFragment();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    //USER LOCATION RETRIEVING
    public String getLocation(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocation(lat, lon, 1);
            if (addressList.size() > 0) {

                userLocation = addressList.get(0).getLocality();
                if (userLocation == null) {
                    userLocation = addressList.get(0).getSubAdminArea();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userLocation;
    }

    //FETCHING LOCATION TURFS
    private class AsyncLoadingTurfsProcess extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            getTurfsByLocation(userLocation);
            return "done";
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        try {
                            getLocation(location.getLatitude(), location.getLongitude());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(this, "NO PERMISSION GRANTED", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //COMMON FRAGMMENT LOAD FUNCTION FOR BASE NAVIGATOR
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_holder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //LOADING BOARDING SCREEN FRAGMENT
    @Override
    public void showBoardingScreens() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        OnBoardingFragment onBoardingFragment = new OnBoardingFragment(getApplicationContext());
        fragmentTransaction.replace(R.id.fragment_holder, onBoardingFragment, "app-onboard-fragment").addToBackStack("app-onboard");
        fragmentTransaction.commit();
    }

    //INITIAL LOAD PAGE FRAGMENT
    private void openLoadingPage() {
        setNavigationHidden();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_holder, new AppStartFragment(getApplicationContext()));
        ft.commit();
    }

    //LOGIN PAGE FRAGMENT
    @Override
    public void openLoginFragment() {
        //Clear all Existing data from Shared Perferences
        clearSharedPreferences();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        AppLoginFragment loginFragment = new AppLoginFragment(this, getApplicationContext());
        fragmentTransaction.replace(R.id.fragment_holder, loginFragment, "app-login-fragment").addToBackStack("login");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void clearSharedPreferences() {
        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    //HOME FRAGMENT
    @Override
    public void openHomeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        HomeFragment homeFragment = new HomeFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, homeFragment, "app-home-fragment").addToBackStack("app-home");
        fragmentTransaction.commit();
    }

    //SUMMARY FRAGMENT
    @Override
    public void openSummaryfragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        SummaryFragment summaryFragment = new SummaryFragment(getSupportFragmentManager(), getApplicationContext());
        fragmentTransaction.replace(R.id.fragment_holder, summaryFragment, "app-summary-fragment").addToBackStack("app-summary");
        fragmentTransaction.commit();
    }

    //TURF VIEW FRAGMENT
    @Override
    public void openTurfDetailsFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        ViewFragment viewFragment = new ViewFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, viewFragment, "app-view-fragment").addToBackStack("app-turf-view");
        fragmentTransaction.commit();
    }

    //OFFER VIEW FRAGMENT
    @Override
    public void openOfferFragments() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        OffersFragment offersFragment = new OffersFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, offersFragment, "app-offer-fragment").addToBackStack("app-offer-view");
        fragmentTransaction.commit();
    }

    //SLOTS VIEW FRAGMENT
    @Override
    public void openTurfSlots() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        SlotsFragment slotsFragment = new SlotsFragment(getSupportFragmentManager(), getApplicationContext());
        fragmentTransaction.replace(R.id.fragment_holder, slotsFragment, "app-slots-fragment").addToBackStack("app-slots");
        fragmentTransaction.commit();
    }

    //OPEN REGISTER FRAGMENT
    @Override
    public void openRegisterFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        RegisterFragment registerFragment = new RegisterFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, registerFragment, "app-register-fragment").addToBackStack("app-register");
        fragmentTransaction.commit();
    }

    //OPEN LOADING PAGE
    @Override
    public void showLoading() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        LoadingFragment loadingFragment = new LoadingFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, loadingFragment, "app-loading-fragment").addToBackStack("app-loading");
        fragmentTransaction.commit();
    }

    //OPEN TRANSACTION FRAGMENT
    @Override
    public void openTransactionsFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        TransactionsFragment transactionsFragment = new TransactionsFragment(getSupportFragmentManager());
        fragmentTransaction.replace(R.id.fragment_holder, transactionsFragment, "app-user-transactions-fragment").addToBackStack("app-transactions");
        fragmentTransaction.commit();
    }

    //SETTING SELECTED TURF
    @Override
    public void setSelectedTurf(Turfs sTurf) {
        selectedTurf = sTurf;

        //SETTSING IMAGES FOR TURF VIEW SLIDER
        this.slideImages = this.selectedTurf.getTurfImages();

        //FETCHING ALL BOOKINGS FOR SELECTED TURF
        getAllBookingsForSelectedTurf(selectedBookingDate);
    }

    //REMOVING SLOTS BOOKED FROM ALL SLOTS OF SELECTED TURF
    void updateSlots() {
        selectedTurfSlots = new ArrayList<>();

        //FETCHING ALL SLOTS OF TURF
        Map<String, Map<String, String>> slots = selectedTurf.getSlots();

        Log.e(TAG,"************************************************************");
        if (slots != null) {
            //FETCHING SLOTS AVAILABLE IN THE TURF
            Set<String> slotids = slots.keySet();
            Log.e(TAG, "total slots in turfs");
            Log.e(TAG, slotids + "");
            Log.e(TAG, "total booked slots in turfs");
            Log.e(TAG, bookedList.size() + "");
            for (Iterator<String> it = slotids.iterator(); it.hasNext(); ) {
                String id = it.next();
                boolean status = false;
                for (int i = 0; i < bookedList.size(); i++) {
                    if (bookedList.get(i).getBookingSlot().equalsIgnoreCase(id)) {
                        status = true;
                    }
                }
                if (!status) {
                    selectedTurfSlots.add(slots.get(id));
                }
            }

//            Log.e(TAG, "selectedTurfSlots size : " + selectedTurfSlots.size() + "");
//            Log.e(TAG, "bookedList size : " + bookedList.size() + "");
//            Log.e(TAG, "" + bookedList);
//            Log.e(TAG, "selected urf ID " + selectedTurf.getTurfID());
//
//            Log.e(TAG, "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        }
    }

    //GETTING SELECTED TURF
    @Override
    public Turfs getSelectedTurf() {
        return selectedTurf;
    }

    //GETTING ALL SLOTS IN SELECTED TURF
    @Override
    public List<Map<String, String>> getSelectedTurfAllSlots() {
        return selectedTurfSlots;
    }

    //LIST OF TURFS IN A LOCATION - GETTER
    @Override
    public List<Turfs> getLocationTurfs() {
        return locationTurfs;
    }

    //FETCH CURRENT USER LOCATION
    @Override
    public String getUserLocation() {
        return userLocation;
    }

    //SETTING USER LOCATION
    @Override
    public void setUserLocation(String uLocation) {
        userLocation = uLocation;
    }

    //GETTING TURFS WITH SELECTED SLOTS AVAILABLE ON PARTICULAR DATE
    @Override
    public void getTurfsWithDateANDTime(final String time) {
        final List<Bookings> tempBookList = new ArrayList<>();
        db.collection("bookings")
                .whereEqualTo("bookingDate", turfFilterDate)
                .whereEqualTo("bookingSlot", time)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Bookings booking = document.toObject(Bookings.class);
                                tempBookList.add(booking);
                            }
                            getRemainingTurfs(tempBookList, time);
                        } else {

                        }
                    }
                });
    }

    //TURFS FILTERED
    private void getRemainingTurfs(List<Bookings> tempBookList, String time) {
        locationDateTimeTurfList = parentLocationData;
        List<Turfs> tempList = new ArrayList<>();
//        Log.e(TAG, "****************************************************8");
//        Log.e(TAG, "temp booked list for the  after getTurfsWithDateANDTime");
//        Log.e(TAG, "size tempBooklist " + tempBookList);
//        Log.e(TAG, tempBookList + "");
//        Log.e(TAG, locationDateTimeTurfList.size() + " : initlal temp list count ");
//        Log.e(TAG, "filter loop begins");
//        Log.e(TAG, "Slot checking for " + time);
        Turfs tempTurf = new Turfs();
        if (tempBookList.size() > 0) {
//            Log.e(TAG, "if condition");
            for (int i = 0; i < tempBookList.size(); i++) {
                String turfID = tempBookList.get(i).getTurfID();

                for (int j = 0; j < locationDateTimeTurfList.size(); j++) {
                    if (!locationDateTimeTurfList.get(j).getTurfID().equalsIgnoreCase(turfID) || locationDateTimeTurfList.get(j).getSlots().get(time) != null) {
                        tempTurf = locationDateTimeTurfList.get(j);
                        tempList.add(tempTurf);
                        tempTurf = new Turfs();

                    }
                }
            }
        } else {
            boolean status = false;
            for (int j = 0; j < locationDateTimeTurfList.size(); j++) {

                if (locationDateTimeTurfList.get(j).getSlots().get(time) != null) {
                    tempTurf = locationDateTimeTurfList.get(j);
                    tempList.add(tempTurf);
                    tempTurf = new Turfs();
                }
            }
        }
        locationTurfs = tempList;
//        Log.e(TAG, locationDateTimeTurfList.size() + " : final temp list count ");
//        Log.e(TAG, "+++++++++++++++++++++++++++++++++++++++++++++++++");
        for (int j = 0; j < locationTurfs.size(); j++) {
//            Log.e(TAG, locationTurfs.get(j).getTurfName());
        }
//        Log.e(TAG, "+++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    //FETCHING SLIDER IMAGES
    @Override
    public List<String> getSlideImages() {
        return slideImages;
    }

    //GETTING EXISTING LOCATIONS OF TURFS
    @Override
    public Set<String> getExistingTurfsLocation() {
        return locationList;
    }

    //NAVIGATION HIDDEN
    @Override
    public void setNavigationHidden() {
        navigation.setVisibility(View.INVISIBLE);
    }

    //NAVIGATION OPENER
    @Override
    public void setNavigationOpen() {
        navigation.setVisibility(View.VISIBLE);
    }

    //GETTING ALL TURFS
    public List<Turfs> gettingAllTurfs() {
        final List<Turfs> completeTurfs = new ArrayList<>();
        db.collection("turfs")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Turfs turf = document.toObject(Turfs.class);

                                String location = turf.getTurfLocality();
                                if (locationList.size() > 0) {
                                    if (locationList.contains(location)) {
                                    } else {
                                        locationList.add(location);
                                    }
                                } else {
                                    locationList.add(location);
                                }
                                completeTurfs.add(turf);
                            }
                        } else {
                        }
                    }
                });
        return completeTurfs;
    }

    //GETTING TURFS BASED ON LOCATION
    @Override
    public void getTurfsByLocation(String userLocation) {
        locationTurfs = new ArrayList<>();
        db.collection("turfs")
                .whereEqualTo("turfLocality", userLocation)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Turfs turf = document.toObject(Turfs.class);
                                locationTurfs.add(turf);
                            }
                        } else {
                        }
                        //FOR BACKWARD COMPATABILITY OF DATA
                        parentLocationData = locationTurfs;
                        checkHomePageData(locationTurfs);
                    }
                });
    }

    private void checkHomePageData(List<Turfs> locationTurfs) {
        if (locationTurfs.size() > 0) {
            openHomeFragment();
        } else {
            userLocation = "Kozhikode";
            new AsyncLoadingTurfsProcess().execute();
        }
    }

    //GETTINGTURF DETAILS BY TURF ID
    public Turfs getTurfByID(String turfID) {
        final Turfs[] turf = {new Turfs()};
        DocumentReference docRef = db.collection("turfs").document(turfID);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {

            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                turf[0] = documentSnapshot.toObject(Turfs.class);
            }
        });
        return turf[0];
    }

    //GETTING ALL BOOKED SLOTS FOR THE TURF
    @Override
    public void getAllBookingsForSelectedTurf(Date date) {

        bookedList = new ArrayList<>();
        db.collection("bookings")
                .whereEqualTo("bookingDate", date)
                .whereEqualTo("turfID", selectedTurf.getTurfID())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Bookings bking = document.toObject(Bookings.class);

                                bookedList.add(bking);
                            }
                        } else {
//                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                        updateSlots();
                    }
                });

    }

    //ADD SLOT TO USER SELECTED SLOTS LIST
    @Override
    public void addTOSelctedSlot(Map<String, String> slotID) {
        slotID.put("bookingDate", selectedBookingDate.toLocaleString());
        userSelectedSlots.add(slotID);
    }

    //REMOVE SLOT FROM USER SELECTED SLOTS LIST
    @Override
    public void removeFROMSelectedSlot(Map<String, String> slotID) {
        userSelectedSlots.remove(slotID);
    }

    //GET USER SELECTED SLOTS
    @Override
    public Set<Map<String, String>> getUserSelectedSlots() {
        return userSelectedSlots;
    }

    //FLUSH USER SELECTED SLOS
    @Override
    public void clearUserSelectedSlots() {
        this.userSelectedSlots.clear();
    }

    //SET DATE OF BOOKING
    @Override
    public void setDateOfBookingOfSelectedSlots(Date selectedDate) {
        selectedBookingDate = selectedDate;
    }

    //GET DATE OF BOOKING
    @Override
    public Date getDateOfBookingOfSelectedSlots() {
        return selectedBookingDate;
    }

    //REGISTER A BOOKING
    @Override
    public void registerBookings() {
        CollectionReference Bookings = db.collection("bookings");
//        Log.e(TAG,"button pressed");
        Toast.makeText(getApplicationContext(), "CONFIRMATION SUCCESSFULL", Toast.LENGTH_SHORT).show();
//        Log.e(TAG,"button pressed "+userSelectedSlots.size());
//        Log.e(TAG,"button pressed "+userSelectedSlots);
        if (userSelectedSlots.size() > 0) {
            Customer cust = getUserDetails();

            Random random = new Random(System.nanoTime() % 100000);
            String bookingOTP = String.format("%04d", random.nextInt(10000));
            String offerCodeApplied = "";
            if (userAppliedOffer == null) {
                offerCodeApplied = "NO-OFFER";
            } else {
                userAppliedOffer.getOfferID();
            }
            for (Iterator<Map<String, String>> it = userSelectedSlots.iterator(); it.hasNext(); ) {

                Map<String, String> slot = it.next();
                Map<String, Object> bookingSlot = new HashMap<>();
                bookingSlot.put("advanceAmount", 500);
                bookingSlot.put("bookingDate", selectedBookingDate);
                bookingSlot.put("bookingSlot", slot.get("slotTag"));
                bookingSlot.put("bookingStatus", false);
                bookingSlot.put("custID", cust.getContact());
                bookingSlot.put("offerCode", offerCodeApplied);
                bookingSlot.put("otp", Integer.parseInt(bookingOTP));
                bookingSlot.put("totalAmount", Integer.parseInt(slot.get("slotCost")));
                bookingSlot.put("turfID", selectedTurf.getTurfID());
                Bookings.document().set(bookingSlot);
//                Log.e(TAG,"booking id : "+Bookings.document().getId());
            }
            sendSMS(cust.getContact(), cust.getName(), bookingOTP);

        }
    }

    //LOADING ALL OFFERS
    @Override
    public List<Offers> loadAllOffers() {
        allOffers = new ArrayList<Offers>();
        db.collection("offers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Offers offer = document.toObject(Offers.class);
                                allOffers.add(offer);
                            }

                        } else {
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        return allOffers;
    }


    @Override
    public List<Offers> getAllOffers() {
        return allOffers;
    }

    @Override
    public List<Turfs> getAllRegisteredTurfs() {
        return allRegisteredTurfs;
    }

    @Override
    public void loadHomeView() {
        new AsyncLoadingTurfsProcess().execute();
    }

    @Override
    public void showComingSoon() {
        Toast.makeText(getApplicationContext(), "COMING SOON", Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getNextFragment() {
        return nextFragment;
    }

    @Override
    public void setNextFragment(String fragment) {
        nextFragment = fragment;
    }

    @Override
    public void setUserContact(String uContact) {
        userContact = uContact;
    }

    //USER REGISTRATION
    @Override
    public void saveUserRegistration(String name, String mail) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplication());
        SharedPreferences.Editor edit = sp.edit();
        userContact = sp.getString("userContact", "");
        CollectionReference customers = db.collection("customers");
        Map<String, Object> customerBk = new HashMap<>();
        customerBk.put("name", name);
        customerBk.put("email", mail);
        customerBk.put("soOfferStatus", false);
        customerBk.put("contact", userContact);
        customers.document(userContact).set(customerBk);
        setNextFragment("Home");

        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(customerBk);
        prefsEditor.putString("logged-in-user", json);
        prefsEditor.commit();

        showLoading();
    }

    @Override
    public Customer getUserDetails() {
        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("logged-in-user", "");
        userContact = mPrefs.getString("userContact", "");
        Customer customerBk = gson.fromJson(json, Customer.class);
        return customerBk;
    }

    @Override
    public void setAppliedOffer(Offers offer) {
        userAppliedOffer = offer;
    }

    @Override
    public List<Bookings> getAllUserBookings() {
        return userBookings;
    }

    @Override
    public void fetchAllUserTransactions() {
        userBookings = new ArrayList<>();
        db.collection("bookings")
                .whereEqualTo("custID", userContact)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Log.e(TAG, "user " + document.getId());
//                                Log.e(TAG, "user " + document.get("turf_id"));

                                Bookings bking = document.toObject(Bookings.class);
//                                Log.e(TAG, "user " + bking.getTurfID());
                                userBookings.add(bking);
                            }
                        } else {
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    @Override
    public Offers getOfferByID(String offerCode) {
//        Log.e(TAG, "offer code : " + offerCode);
        DocumentReference docRef = db.collection("offers").document(offerCode);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {

            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                appliedOfferDt[0] = documentSnapshot.toObject(Offers.class);
            }

        });
        return appliedOfferDt[0];
    }

    @Override
    public Offers getAppliedOffer() {
        return appliedOfferDt[0];
    }

    public void loadAllRegisteredTurfs() {
        allRegisteredTurfs = new ArrayList<Turfs>();
        db.collection("turfs")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Turfs turf = document.toObject(Turfs.class);
                                allRegisteredTurfs.add(turf);
                            }
                        } else {
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
        if (currentFragment instanceof AppLoginFragment || currentFragment instanceof HomeFragment) {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        } else {
            super.onBackPressed();
        }
    }

    void sendSMS(String contact, String name, String code) {

        HttpClient client = ApiClient.getClient().create(HttpClient.class);
        Call<OTPResponse> response = client.sendOTPwithTemplete("TRANS_SMS",
                "49b9925c-0e43-11e9-a895-0200cd936042", contact,
                "SWTOUT",
                "Booking_Success", name
                , code);

        response.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                Log.e(TAG, "reponse of messasge");
                Log.e(TAG, response.body().getStatus() + response.body().getDetails());
                System.out.println(response.body().getStatus() + response.body().getDetails());
                Toast.makeText(getApplicationContext(), "CONFIRMATION SUCCESSFULL", Toast.LENGTH_SHORT).show();
                loadHomeView();
            }

            @Override
                public void onFailure(Call<OTPResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "CONFIRMATION SMS SENDING FAILED Please Contact Check and Confirm in Account Section", Toast.LENGTH_SHORT).show();
                System.out.println(t);
                Log.e(TAG, "reponse of messasge");
                Log.e(TAG, t.getMessage()+" + "+t.getStackTrace()+" + "+t.getLocalizedMessage());
            }
        });

//        String module = "TRANS_SMS";
//        String apikey = "49b9925c-0e43-11e9-a895-0200cd936042";
//        String to = contact;
//        String from = "SWTOUT";
//        String templatename = "Booking_Success";
//        String var1 = name;
//        String var2 = code;
//
//        smsclient = ApiClient.getClient().create(HttpClient.class);
//
//        Call<OTPResponse> response = smsclient.sendOTPwithTemplete(module, apikey, to, from, templatename, var1, var2);
//        response.enqueue(new Callback<OTPResponse>() {
//            @Override
//            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
//                Log.e(TAG, "response on message sending  " + response);
//                if (response.code() == 200) {
//                    Toast.makeText(getApplicationContext(), "CONFIRMATION SUCCESSFULL", Toast.LENGTH_SHORT).show();
//                    loadHomeView();
//                } else {
//                    Toast.makeText(getApplicationContext(), "CONFIRMATION SMS SENDING FAILED Please Contact Check and Confirm in Account Section", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<OTPResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "OOPS SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
//                System.out.println(t);
//            }
//        });
    }

    @Override
    public Date getDateFromString(String s) {
        String myFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        try {
            Date date = sdf.parse(s);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Date settingDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 12);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        horizontalCalenderPreDate = cal;
        return cal.getTime();
    }

    @Override
    public void setDateForTurfFiltering(Date dtSelected) {
        turfFilterDate = dtSelected;
    }

    @Override
    public Calendar getHorizontalDateSetter() {
        return horizontalCalenderPreDate;
    }
}
