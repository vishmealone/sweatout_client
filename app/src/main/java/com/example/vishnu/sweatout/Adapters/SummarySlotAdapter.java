package com.example.vishnu.sweatout.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by vishmealone on 14-11-2018.
 */

class SummarySlotLoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public SummarySlotLoadingViewHolder(View itemView) {
        super(itemView);
    }
}

class SummarySlotViewHolder extends RecyclerView.ViewHolder {

    public TextView  slotTime, slotCost,dateValue;
    public LinearLayout cardDataContainerLayout;

    public SummarySlotViewHolder(View itemView) {
        super(itemView);

        slotTime = (TextView) itemView.findViewById(R.id.timeValue);
        slotCost = (TextView) itemView.findViewById(R.id.slotValue);
        dateValue = (TextView) itemView.findViewById(R.id.dateValue);

//        cardDataContainerLayout = itemView.findViewById(R.id.cardDataContainerLayout);
    }
}

public class SummarySlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    MainInterface mainInterface;

    boolean isLoading;
    Context context;
    List<Map<String, String>> items;
    int lastVisibleItem, totalItemCount;

    public SummarySlotAdapter(RecyclerView recyclerView, Context context, List<Map<String, String>> items) {


        this.context = context;
        this.items = items;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycler_slot_summary, parent, false);
            return new SummarySlotViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycle_loading, parent, false);
            return new SummarySlotViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SummarySlotViewHolder) {
            mainInterface = (MainInterface) context;
            final Map<String, String> slot = items.get(position);
            final SummarySlotViewHolder viewHolder = (SummarySlotViewHolder) holder;
            viewHolder.slotCost.setText("Rs "+slot.get("slotCost"));
            viewHolder.slotTime.setText(slot.get("slotTime"));
            String slotDate = slot.get("bookingDate");

            viewHolder.dateValue.setText(slotDate);
        } else if (holder instanceof SummarySlotLoadingViewHolder) {
            SummarySlotLoadingViewHolder loadingViewHolder = (SummarySlotLoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

}

