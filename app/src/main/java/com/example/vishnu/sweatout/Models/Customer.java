package com.example.vishnu.sweatout.Models;

public class Customer {

    private String name;
    private String email;
    private String contact;
    private boolean soOfferStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean isSoOfferStatus() {
        return soOfferStatus;
    }

    public void setSoOfferStatus(boolean soOfferStatus) {
        this.soOfferStatus = soOfferStatus;
    }
}
