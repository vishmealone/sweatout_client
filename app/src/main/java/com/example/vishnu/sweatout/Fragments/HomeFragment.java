package com.example.vishnu.sweatout.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.vishnu.sweatout.Adapters.CustomViewPagerAdapter;
import com.example.vishnu.sweatout.Adapters.ViewAdapter;
import com.example.vishnu.sweatout.Common.SelectDateFragment;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Turfs;
import com.example.vishnu.sweatout.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment {

    private static String TAG = "sweatout";
    private MainInterface mainInterface;
    private LinearLayout locationHeader, offerLinearLayout, recyclerContainer,noTurfsFound;


    Set<String> slotList = new HashSet<>();
    List<Map<String, String>> slotMapList = new ArrayList<>();

    private RadioGroup rg, rg_slot;
    private Handler handler;
    private ViewPager viewPager;
    private ViewAdapter viewAdapter;
    ProgressDialog progressDialog;

    RecyclerView recyclerView;

    private TextView locationText, editTextDate, editTextTime;
    private static final long SLIDER_TIMER = 2000;
    private int currentPage = 0;

    FragmentManager fragmentManager;
    private boolean isCountDownTimerActive = false; // let the timer start if and only if it has completed previous task

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!isCountDownTimerActive) {
                automateSlider();
            }
            handler.postDelayed(runnable, 1000);
        }
    };

    public HomeFragment() {
    }

    public HomeFragment(FragmentManager supportFragmentManager) {
        fragmentManager = supportFragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.turfListRecycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mainInterface.setNavigationOpen();

        offerLinearLayout = (LinearLayout) view.findViewById(R.id.offerLinearLayout);
        recyclerContainer = (LinearLayout) view.findViewById(R.id.recyclerContainer);
        noTurfsFound = (LinearLayout) view.findViewById(R.id.noTurfsFound);


        //SETTING LOCATION TO HOME PAGE
        locationText = (TextView) view.findViewById(R.id.locationText);
        editTextDate = (TextView) view.findViewById(R.id.editTextDate);
        editTextTime = (TextView) view.findViewById(R.id.editTextTime);

        //OFFER PAGE LOADER
        offerLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.openOfferFragments();
            }
        });

        locationText.setText("   " + mainInterface.getUserLocation());
        locationHeader = (LinearLayout) view.findViewById(R.id.locationHeader);

        //SETTING SLIDER
        viewPager = (ViewPager) view.findViewById(R.id.view_pager_slider);
        CustomViewPagerAdapter viewPagerAdapter = new CustomViewPagerAdapter(fragmentManager);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    currentPage = 0;
                } else if (position == 1) {
                    currentPage = 1;
                } else {
                    currentPage = 2;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

        });

        handler = new Handler();
        handler.postDelayed(runnable, 1000);
        runnable.run();

        final Dialog slot_dialog = new Dialog(getActivity());
        slot_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        slot_dialog.setContentView(R.layout.time_slot_list);

        rg_slot = (RadioGroup) slot_dialog.findViewById(R.id.radio_group_slot);

        createAllPossibleSlots();

        creataAllSlotMapList();

        for (Iterator<String> it = slotList.iterator(); it.hasNext(); ) {
            RadioButton rb = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setText(it.next());
            rb.setPadding(15, 15, 15, 15);
            rb.setWidth(600);
            rb.setTextColor(Color.WHITE);
            rb.setHeight(120);
            rg_slot.addView(rb);
        }


        //DATE PICKER
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment(editTextDate);
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //SLOT TIME PICKER
        editTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slot_dialog.show();
            }
        });

        //TIME LOADER AND SELECTOR
        rg_slot.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                    RadioButton btn = (RadioButton) group.getChildAt(x);
                    if (btn.getId() == checkedId) {
                        String timeslot = btn.getText().toString();
                        editTextTime.setText("   " + timeslot);
                        if (!editTextDate.getText().toString().equalsIgnoreCase("   Date")
                                && !editTextTime.getText().toString().equalsIgnoreCase("   Time")) {
                            String selectTime = "";
                            for (int i = 0; i < slotMapList.size(); i++) {
                                if (slotMapList.get(i).get(timeslot) != null) {
                                    selectTime = slotMapList.get(i).get(timeslot);
                                }
                            }
                            Date selectedDat = mainInterface.getDateOfBookingOfSelectedSlots();
                            Date selectFinalDate = mainInterface.settingDate(selectedDat);
                            mainInterface.setDateOfBookingOfSelectedSlots(selectFinalDate);
                            getTurfsListForDateANDTime(selectTime);
                        }
                        slot_dialog.hide();
                    }
                }
            }

        });


        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.location_list_dialog);

        rg = (RadioGroup) dialog.findViewById(R.id.radio_group);

        Set<String> locationList = mainInterface.getExistingTurfsLocation();
        if (rg.getChildCount() > 0) {
            for (int i = 0; i < rg.getChildCount(); i++) {
                rg.removeViewAt(i);
            }
        }

        for (Iterator<String> it = locationList.iterator(); it.hasNext(); ) {
            RadioButton rb = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setText(it.next());
            rb.setPadding(15, 15, 15, 15);
            rb.setWidth(600);
            rb.setTextColor(Color.WHITE);
            rb.setHeight(120);
            rg.addView(rb);
        }

        locationHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        //CHANGE OF LOCATION
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                    RadioButton btn = (RadioButton) group.getChildAt(x);
                    if (btn.getId() == checkedId) {
                        String location = btn.getText().toString();
                        locationText.setText(location);
                        mainInterface.setUserLocation(btn.getText().toString());
                        mainInterface.getTurfsByLocation(location);
                        dialog.hide();
                    }
                }
            }
        });

        mainInterface.setUserLocation("Kozhikode");
        List<Turfs> turfsList = new ArrayList<>();
        turfsList = mainInterface.getLocationTurfs();
//        Log.e(TAG,"location turf List size : "+turfsList);
        getTurfsForRecycler(turfsList);

        mainInterface.fetchAllUserTransactions();
        return view;
    }

    private void creataAllSlotMapList() {
        slotMapList = new ArrayList<>();
        Map<String, String> slot = new HashMap<>();
        slot.put("6am - 7am", "6AM7AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("7am - 8am", "7AM8AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("8am - 9am", "8AM9AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("9am - 10am", "9AM10AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("10am - 11am", "10AM11AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("11am - 12am", "11AM12AM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("12am - 1pm", "12AM1PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("1pm - 2pm", "1PM2PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("2pm - 3pm", "2PM3PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("3pm - 4pm", "3PM4PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("4pm - 5pm", "4PM5PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("5pm - 6pm", "5PM6PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("6pm - 7pm", "6PM7PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("7pm - 8pm", "7PM8PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("8pm - 9pm", "8PM9PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("9pm - 10pm", "9PM10PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("10pm - 11pm", "10PM11PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("11pm - 12pm", "11PM12PM");
        slotMapList.add(slot);
        slot = new HashMap<>();
        slot.put("12pm - 1Am", "11PM1AM");
        slotMapList.add(slot);
    }

    private void createAllPossibleSlots() {
        slotList = new HashSet<>();
        slotList.add("6am - 7am");
        slotList.add("7am - 8am");
        slotList.add("8am - 9am");
        slotList.add("9am - 10am");
        slotList.add("11am - 12am");
        slotList.add("12am - 1pm");
        slotList.add("1pm - 2pm");
        slotList.add("2pm - 3pm");
        slotList.add("3pm - 4pm");
        slotList.add("4pm - 5pm");
        slotList.add("5pm - 6pm");
        slotList.add("6pm - 7pm");
        slotList.add("7pm - 8pm");
        slotList.add("8pm - 9pm");
        slotList.add("9pm - 10pm");
        slotList.add("10pm - 11pm");
        slotList.add("11pm - 12pm");
        slotList.add("12pm - 1am");
    }

    private void getTurfsListForDateANDTime(String time) {
        mainInterface.getTurfsWithDateANDTime(time);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMax(100); // Progress Dialog Max Value
        progressDialog.setMessage("Refreshing Turfs"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Horizontal
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        final Handler handler = new Handler();
//        mainInterface.showLoading();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//            mainInterface.loadHomeView();
                getTurfsForRecycler(mainInterface.getLocationTurfs());
                progressDialog.hide();

            }
        }, 4000);
    }

    private void getTurfsForRecycler(List<Turfs> turfs) {
        if (turfs.size() > 0) {
            noTurfsFound.setVisibility(View.INVISIBLE);
            recyclerContainer.setVisibility(View.VISIBLE);
            viewAdapter = new ViewAdapter(recyclerView, getActivity(), turfs);
            viewAdapter.notifyDataSetChanged();
            recyclerView.setAdapter(viewAdapter);
        }
        else{
            recyclerContainer.setVisibility(View.GONE);
            noTurfsFound.setVisibility(View.VISIBLE);
        }
    }

    private void automateSlider() {
        isCountDownTimerActive = true;
        new CountDownTimer(SLIDER_TIMER, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                int nextSlider = currentPage + 1;
                if (nextSlider == 3) {
                    nextSlider = 0; // if it's last Image, let it go to the first image
                }
                viewPager.setCurrentItem(nextSlider);
                isCountDownTimerActive = false;
            }
        }.start();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

}
