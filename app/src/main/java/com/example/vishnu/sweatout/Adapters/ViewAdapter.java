package com.example.vishnu.sweatout.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Turfs;
import com.example.vishnu.sweatout.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Created by vishmealone on 14-11-2018.
 */

class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public LoadingViewHolder(View itemView) {
        super(itemView);
//        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}

class ItemViewHolder extends RecyclerView.ViewHolder {

    public TextView turfName, playerCount,starCount,costValue;
    public LinearLayout imageLayout, cardDataContainerLayout;

    public ItemViewHolder(View itemView) {
        super(itemView);

        turfName = (TextView) itemView.findViewById(R.id.turfName);
        playerCount = (TextView) itemView.findViewById(R.id.playerCount);
        starCount = (TextView) itemView.findViewById(R.id.starCount);
        costValue = (TextView) itemView.findViewById(R.id.costValue);
        imageLayout = (LinearLayout) itemView.findViewById(R.id.imageLayout);
        cardDataContainerLayout = (LinearLayout) itemView.findViewById(R.id.cardDataContainerLayout);

    }
}

public class ViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    MainInterface mainInterface;

    boolean isLoading;
    Context context;
    List<Turfs> items;
    int visibleThreshold = 5;
    int lastVisibleItem, totalItemCount;

    public ViewAdapter(RecyclerView recyclerView, Context context, List<Turfs> items) {
        this.context = context;
        this.items = items;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycler_item, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycle_loading, parent, false);
            return new ItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {


            final Turfs turfsPreviewModel = items.get(position);
            ItemViewHolder viewHolder = (ItemViewHolder) holder;
            if (turfsPreviewModel != null) {
                viewHolder.turfName.setText(turfsPreviewModel.getTurfName());
                viewHolder.starCount.setText("  "+String.valueOf(turfsPreviewModel.getTurfRating()));
                viewHolder.costValue.setText("  "+String.valueOf(turfsPreviewModel.getTurf_Cost()));
                viewHolder.playerCount.setText("  "+String.valueOf(turfsPreviewModel.getTurfCapacity()));
                String image = turfsPreviewModel.getTurfImages().get(0);//"https://www.thesoccerstore.co.uk/blog/wp-content/uploads/2015/09/football-match-600x400.jpg";//
//                Picasso.with(context).load(image).into(((ItemViewHolder) holder).imageView);
                Picasso.with(context).load(image).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        ((ItemViewHolder) holder).imageLayout.setBackground(new BitmapDrawable(context.getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

                viewHolder.cardDataContainerLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainInterface = (MainInterface) context;
                        mainInterface.setSelectedTurf(turfsPreviewModel);
                        mainInterface.openTurfDetailsFragment();
                    }

                });
            }
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

}

