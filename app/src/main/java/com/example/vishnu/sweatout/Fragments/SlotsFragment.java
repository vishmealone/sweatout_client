package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.vishnu.sweatout.Adapters.SlotAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SlotsFragment extends Fragment {

    private static String TAG = "sweatout";
    Context context;
    View view;
    private MainInterface mainInterface;
    private RecyclerView recyclerView;
    private SlotAdapter slotAdapter;
    private Button bookNowButton;
    private Date selectedDate;
    private Set<String> selected = new HashSet<>();
    private List<Map<String, String>> turfslots = new ArrayList<>();
    ProgressDialog progressDialog;


    public SlotsFragment(FragmentManager supportFragmentManager, Context mContext) {
        this.context = mContext;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_slots, container, false);


        mainInterface.clearUserSelectedSlots();
        //ALL AVAILABLE SLOTS FOR THE TURFS
        turfslots = mainInterface.getSelectedTurfAllSlots();

        Log.e(TAG,"in slots fragment + "+ turfslots.size());
        Log.e(TAG,""+ turfslots);
        recyclerView = (RecyclerView) view.findViewById(R.id.slotListRecycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //SETTING INITIAL RECYCLER
        setRecyclerAdapterforSlots();
        mainInterface.setNavigationHidden();

        final Handler handler = new Handler();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);

//        Calendar selectedDt = Calendar.getInstance();
//        selectedDt.add();

        final Calendar selectedDt= mainInterface.getHorizontalDateSetter();


//        Calendar calenderDate =
        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .defaultSelectedDate(selectedDt)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
               Date dt = date.getTime();
               selectedDate = mainInterface.settingDate(dt);
               mainInterface.setDateOfBookingOfSelectedSlots(selectedDate);
//                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
//                String slDate = format1.format(ndate);
//                selectedDate = null;
//                try {
//                    selectedDate = format1.parse(slDate);
//                } catch (ParseException e1) {
//                    e1.printStackTrace();
//                }

                turfslots.clear();
                mainInterface.setDateOfBookingOfSelectedSlots(selectedDate);
                mainInterface.getAllBookingsForSelectedTurf(selectedDate);
                recyclerView.setVisibility(View.INVISIBLE);


                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMax(100); // Progress Dialog Max Value
                progressDialog.setMessage("Loading Slots"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Horizontal
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        turfslots = mainInterface.getSelectedTurfAllSlots();
                        setRecyclerAdapterforSlots();
                        recyclerView.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }
                }, 3000);

            }
        });

        bookNowButton = (Button) view.findViewById(R.id.bookNowButton);
        bookNowButton.setOnClickListener(mCorkyListener);

        return view;
    }

    private void setRecyclerAdapterforSlots() {
        slotAdapter = new SlotAdapter(recyclerView, getActivity(), turfslots);
        slotAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(slotAdapter);
    }

    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener mCorkyListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (mainInterface.getUserSelectedSlots().size() > 0) {
                if (selectedDate == null) {
                    selectedDate = mainInterface.getDateOfBookingOfSelectedSlots();
                }
                mainInterface.setDateOfBookingOfSelectedSlots(selectedDate);
                mainInterface.openSummaryfragment();
            } else {
                Toast.makeText(context, "Please select any SLOT", Toast.LENGTH_SHORT);
            }
        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

}
