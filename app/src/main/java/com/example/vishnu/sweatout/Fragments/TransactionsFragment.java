package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.vishnu.sweatout.Adapters.OfferViewAdapter;
import com.example.vishnu.sweatout.Adapters.TransactionViewAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Bookings;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class TransactionsFragment extends Fragment {

    private static String TAG = "sweatout";
    MainInterface mainInterface;

    RecyclerView transactionsListRecycleView;
    TransactionViewAdapter transactionsViewAdapter;

    LinearLayout transactionListViewContainer, noTransactionsContainer;
    AsyncLoadingTransactionsProcess asyncLoadingTransactionProcess;

    List<Bookings> allBookingsByUser;


    public TransactionsFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transactions, container, false);
        transactionsListRecycleView = (RecyclerView) view.findViewById(R.id.transactionListRecycleView);

        transactionsListRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        noTransactionsContainer = (LinearLayout) view.findViewById(R.id.noTransactionsContainer);
        transactionListViewContainer = (LinearLayout) view.findViewById(R.id.transactionsListViewContainer);

        new AsyncLoadingTransactionsProcess().execute();

        return view;
    }


    private class AsyncLoadingTransactionsProcess extends AsyncTask<String, String, List<Bookings>> {

        @Override
        protected List<Bookings> doInBackground(String... params) {

            return mainInterface.getAllUserBookings();
        }


        @Override
        protected void onPostExecute(List<Bookings> result) {
            if (result.size() > 0) {
                transactionListViewContainer.setVisibility(View.VISIBLE);
                transactionsViewAdapter = new TransactionViewAdapter(transactionsListRecycleView, getActivity(), result);
                transactionsViewAdapter.notifyDataSetChanged();
                transactionsListRecycleView.setAdapter(transactionsViewAdapter);
            } else {
                noTransactionsContainer.setVisibility(View.VISIBLE);
            }
        }


        @Override
        protected void onPreExecute() {
            noTransactionsContainer.setVisibility(View.INVISIBLE);
            transactionListViewContainer.setVisibility(View.INVISIBLE);
        }


        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

}
