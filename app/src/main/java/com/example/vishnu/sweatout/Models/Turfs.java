package com.example.vishnu.sweatout.Models;

import java.util.List;
import java.util.Map;

public class Turfs {

    String turfID;
    String turfName;
    String turfLocality;
    String turfAddress;
    String turfGLocation;
    boolean turfStatus;
    float turfRating;
    int turfCapacity;
    int turfCost;
    List<String> turfImages;
    Map<String,Boolean> turfFacility;
    Map<String,Map<String,String>> slots;

    public Turfs() {
    }

    public Turfs(String turfID, String turfName) {
        this.turfID = turfID;
        this.turfName = turfName;
    }

    public boolean isTurfStatus() {
        return turfStatus;
    }

    public int getTurfCost() {
        return turfCost;
    }

    public Map<String, Map<String, String>> getSlots() {
        return slots;
    }

    public String getTurfID() {
        return turfID;
    }

    public String getTurfName() {
        return turfName;
    }

    public String getTurfLocality() {
        return turfLocality;
    }

    public float getTurfRating() {
        return turfRating;
    }

    public boolean isTurf_status() {
        return turfStatus;
    }

    public int getTurfCapacity() {
        return turfCapacity;
    }

    public int getTurf_Cost() {
        return turfCost;
    }

    public String getTurfAddress() {
        return turfAddress;
    }

    public String getTurfGLocation() {
        return turfGLocation;
    }

    public List<String> getTurfImages() {
        return turfImages;
    }

    public Map<String, Boolean> getTurfFacility() {
        return turfFacility;
    }
}
