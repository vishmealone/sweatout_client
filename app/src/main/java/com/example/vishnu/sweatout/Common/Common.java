package com.example.vishnu.sweatout.Common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

/**
 * Created by vishnu on 17-11-2018.
 */

public class Common {

    public View view;
    public Context context;

    public MainInterface mainInterface;

    public void setView(View view) {
        this.view = view;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

            NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);

            Snackbar snackbar = Snackbar.make(view, Html.fromHtml("<font color=\"#FF0000\">NETWORK CONNECTION LOST</font>"), Snackbar.LENGTH_INDEFINITE);
            View snackBarView = snackbar.getView();
            snackBarView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            snackBarView.setBackgroundColor(Color.WHITE);
            snackbar.show();

            if (currentNetworkInfo.isConnected()) {
                snackBarView.setVisibility(View.INVISIBLE);
            } else {
                snackBarView.setVisibility(View.VISIBLE);
                new AlertDialog.Builder(context, R.style.Theme_AppCompat)
                        .setTitle("NETWOEK ISSUE")
                        .setMessage(" PLEASE CHECK YOUR CONNECTIVITY ")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        });
            }
        }
    };
}
