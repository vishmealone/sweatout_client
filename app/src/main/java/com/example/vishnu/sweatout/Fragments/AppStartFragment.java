package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AppStartFragment extends Fragment {

    private static String TAG = "sweatout";
    Context context;
    MainInterface mainInterface;
    Handler handler;

    private SharedPreferences.Editor edit;
    private SharedPreferences sp;

    public AppStartFragment(Context applicationContext) {
        this.context = applicationContext;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_app_start, container, false);
        mainInterface.setNavigationHidden();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callNextFragment();
            }
        }, 2000);
        return view;
    }

    private void callNextFragment() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        edit = sp.edit();
        boolean registered = sp.getBoolean(getString(R.string.pref_previously_started), false);

        if (registered) {
            mainInterface.setNextFragment("Home");
            mainInterface.showLoading();

        } else {
            // Check if we need to display our OnboardingFragment
            if (!sharedPreferences.getBoolean(
                    OnBoardingFragment.COMPLETED_ONBOARDING_PREF_NAME, false)) {
                // The user hasn't seen the OnboardingFragment yet, so show it
                mainInterface.showBoardingScreens();

            } else {
//                mainInterface.saveUserRegistration("vishnu","vishnupn@sweatout.in");
                mainInterface.showLoading();
                mainInterface.openLoginFragment();
            }
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }
}
