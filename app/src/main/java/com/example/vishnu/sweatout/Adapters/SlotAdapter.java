package com.example.vishnu.sweatout.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

import java.util.List;
import java.util.Map;

/**
 * Created by vishmealone on 14-11-2018.
 */

class SlotLoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public SlotLoadingViewHolder(View itemView) {
        super(itemView);
//        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}

class SlotViewHolder extends RecyclerView.ViewHolder {

    public TextView slotTime, slotCost;
    public LinearLayout cardDataContainerLayout;
    public ImageView checkImage;

    public SlotViewHolder(View itemView) {
        super(itemView);

        checkImage = (ImageView) itemView.findViewById(R.id.checkImage);
        slotTime = (TextView) itemView.findViewById(R.id.slotTime);
        slotCost = (TextView) itemView.findViewById(R.id.slotCost);
        cardDataContainerLayout = itemView.findViewById(R.id.cardDataContainerLayout);
    }
}

public class SlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    private static String TAG = "sweatout";
    MainInterface mainInterface;

    boolean isLoading;
    Context context;
    List<Map<String, String>> items;
    int lastVisibleItem, totalItemCount;

    public SlotAdapter(RecyclerView recyclerView, Context context, List<Map<String, String>> items) {
        this.context = context;
        this.items = items;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycler_slot, parent, false);
            return new SlotViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycle_loading, parent, false);
            return new SlotViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SlotViewHolder) {
            mainInterface = (MainInterface) context;
            final Map<String, String> slot = items.get(position);
            final SlotViewHolder viewHolder = (SlotViewHolder) holder;
            viewHolder.slotCost.setText("  "+slot.get("slotCost"));
            viewHolder.slotTime.setText(slot.get("slotTime"));
            viewHolder.cardDataContainerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainInterface = (MainInterface) context;
                    if(viewHolder.checkImage.getVisibility()==View.INVISIBLE) {
                        viewHolder.checkImage.setVisibility(View.VISIBLE);
                        mainInterface.addTOSelctedSlot(slot);
                    }
                    else{
                        viewHolder.checkImage.setVisibility(View.INVISIBLE);
                        mainInterface.removeFROMSelectedSlot(slot);
                    }
                }

            });

        } else if (holder instanceof SlotLoadingViewHolder) {
            SlotLoadingViewHolder loadingViewHolder = (SlotLoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

}

