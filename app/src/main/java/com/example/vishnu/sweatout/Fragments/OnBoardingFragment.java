package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vishnu.sweatout.Adapters.OBSliderAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class OnBoardingFragment extends Fragment {

    private static String TAG = "sweatout";
    public static String COMPLETED_ONBOARDING_PREF_NAME = "COMPLETED_ONBOARDING_PREF_NAME";
    Context context;
    ViewPager viewPager;
    LinearLayout dotsLayout;
    Button backButton, nextButton;
    OBSliderAdapter obSliderAdapter;
    MainInterface mainInterface;

    private TextView[] mDots;

    private int[] layouts = {};
    private int currentPage =0;

    public OnBoardingFragment(Context applicationContext) {
        this.context = applicationContext;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_on_boarding, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.slideViewPager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.dotsLayout);

        backButton = (Button) view.findViewById(R.id.prevButton);
        nextButton = (Button) view.findViewById(R.id.nextButton);

        obSliderAdapter = new OBSliderAdapter(context);

        viewPager.setAdapter(obSliderAdapter);
        addDotsIndicator(0);
        viewPager.addOnPageChangeListener(onPageChangeListener);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(currentPage-1);
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPage==2) {
                    onFinishFragment();
                }
                else{
                    viewPager.setCurrentItem(currentPage + 1);
                }
            }
        });

        return view;
    }

    public void addDotsIndicator(int position) {
        mDots = new TextView[3];
        dotsLayout.removeAllViews();
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(context);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(45);
            mDots[i].setGravity(Gravity.CENTER);
            mDots[i].setTextColor(getResources().getColor(R.color.appBaseColor));
            dotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.blue));
        }
    }


    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            currentPage = i;
            if(i==0){
                backButton.setEnabled(false);
                nextButton.setEnabled(true);
                backButton.setVisibility(View.INVISIBLE);
                nextButton.setText("Next");
                backButton.setText("");
            }
            else if(i == mDots.length-1){
                backButton.setEnabled(true);
                nextButton.setEnabled(true);
                backButton.setVisibility(View.VISIBLE);
                nextButton.setText("Finish");
                backButton.setText("Back");
            }
            else{
                backButton.setEnabled(true);
                nextButton.setEnabled(true);
                backButton.setVisibility(View.VISIBLE);
                nextButton.setText("Next");
                backButton.setText("Back");
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

    private void onFinishFragment() {
        // User has seen OnboardingFragment, so mark our SharedPreferences
        // flag as completed so that we don't show our OnboardingFragment
        // the next time the user launches the app.
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        sharedPreferencesEditor.putBoolean(
                COMPLETED_ONBOARDING_PREF_NAME, true);
        sharedPreferencesEditor.apply();
        mainInterface.openLoginFragment();
    }

}
