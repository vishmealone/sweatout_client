package com.example.vishnu.sweatout.Adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vishnu.sweatout.Fragments.SliderOneFragment;
import com.example.vishnu.sweatout.Fragments.SliderThreeFragment;
import com.example.vishnu.sweatout.Fragments.SliderTwoFragment;

public class CustomViewPagerAdapter extends FragmentStatePagerAdapter {

    public CustomViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        if (position == 0) {
            fragment = new SliderOneFragment();
        } else if (position == 1) {
            fragment = new SliderTwoFragment();
        } else {
            fragment = new SliderThreeFragment();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}