package com.example.vishnu.sweatout.Interfaces;

import com.example.vishnu.sweatout.Models.Bookings;
import com.example.vishnu.sweatout.Models.Customer;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.Models.Turfs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface MainInterface {

    void setNavigationOpen();

    void setNavigationHidden();
    
    void openTurfSlots();

    void openLoginFragment();

    void openHomeFragment();

    void openSummaryfragment();

    void openTurfDetailsFragment();

    void openOfferFragments();

    String getUserLocation();

    Date getDateOfBookingOfSelectedSlots();

    Turfs getSelectedTurf();

    List<String> getSlideImages();

    void getTurfsByLocation(String location);

    List<Turfs> getLocationTurfs();

    List<Map<String,String>> getSelectedTurfAllSlots();

    Set<Map<String,String>> getUserSelectedSlots();

    Set<String> getExistingTurfsLocation();

    void getAllBookingsForSelectedTurf(Date date);

    void addTOSelctedSlot(Map<String,String> slot);

    void removeFROMSelectedSlot(Map<String,String> slot);

    void clearUserSelectedSlots();

    void setDateOfBookingOfSelectedSlots(Date selectedDate);

    void registerBookings();

    void setSelectedTurf(Turfs turfsPreviewModel);

    void setUserLocation(String location);

    void getTurfsWithDateANDTime(String time);

    void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername);

    List<Offers> loadAllOffers();

    List<Offers> getAllOffers();

    List<Turfs> getAllRegisteredTurfs();

    void showBoardingScreens();

    void loadHomeView();

    void openTransactionsFragment();

    void showComingSoon();

    void openRegisterFragment();

    void showLoading();

    String getNextFragment();

    void setNextFragment(String home);

    void setUserContact(String userContact);

    void saveUserRegistration(String name, String mail);

    Customer getUserDetails();

    void setAppliedOffer(Offers offer);

    List<Bookings> getAllUserBookings();

    void fetchAllUserTransactions();

    Offers getOfferByID(String offerCode);

    Offers getAppliedOffer();

    Date getDateFromString(String dt);

    Date settingDate(Date date);

    void setDateForTurfFiltering(Date dtSelected);

    Calendar getHorizontalDateSetter();
}
