package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class LoadingFragment extends Fragment {

    Handler handler;
    MainInterface mainInterface;

    public LoadingFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_loading, container, false);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callNextFragment();
            }
        }, 3000);
        return view;
    }

    private void callNextFragment() {
        String next = mainInterface.getNextFragment();

        if(next.equalsIgnoreCase("Home")){
            mainInterface.loadHomeView();
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }
}
