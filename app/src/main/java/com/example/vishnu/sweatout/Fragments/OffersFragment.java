package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.vishnu.sweatout.Adapters.OfferViewAdapter;
import com.example.vishnu.sweatout.Adapters.ViewAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class OffersFragment extends Fragment {

    private static String TAG = "sweatout";

    RecyclerView offerListRecycleView;
    OfferViewAdapter offerViewAdapter;
    MainInterface mainInterface;

    LinearLayout loadingContainer, offerListViewContainer, noOfferContainer;
    AsyncLoadingOfferProcess asyncLoadingOfferProcess;

    List<Offers> allOffers;

    public OffersFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offers, container, false);
        offerListRecycleView = (RecyclerView) view.findViewById(R.id.offerListRecycleView);

        offerListRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        noOfferContainer = (LinearLayout) view.findViewById(R.id.noOfferContainer);
        offerListViewContainer = (LinearLayout) view.findViewById(R.id.offerListViewContainer);
        loadingContainer = (LinearLayout) view.findViewById(R.id.loadingContainer);

        new AsyncLoadingOfferProcess().execute();
        return view;
    }

    private class AsyncLoadingOfferProcess extends AsyncTask<String, String, List<Offers>> {

        @Override
        protected List<Offers> doInBackground(String... params) {
            return mainInterface.getAllOffers();
        }


        @Override
        protected void onPostExecute(List<Offers> result) {
            loadingContainer.setVisibility(View.INVISIBLE);
            if (result.size() > 0) {
                offerListViewContainer.setVisibility(View.VISIBLE);
                offerViewAdapter = new OfferViewAdapter(offerListRecycleView, getActivity(), result);
                offerViewAdapter.notifyDataSetChanged();
                offerListRecycleView.setAdapter(offerViewAdapter);
            } else {
                noOfferContainer.setVisibility(View.VISIBLE);
            }
        }


        @Override
        protected void onPreExecute() {
            loadingContainer.setVisibility(View.VISIBLE);
            noOfferContainer.setVisibility(View.INVISIBLE);
            offerListViewContainer.setVisibility(View.INVISIBLE);
        }


        @Override
        protected void onProgressUpdate(String... text) {
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }
}
