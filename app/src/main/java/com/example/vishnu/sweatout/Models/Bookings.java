package com.example.vishnu.sweatout.Models;

import java.util.Date;

public class Bookings {

    private String turfID;
    private int totalAmount;
    private String offerCode;
    private String custID;
    private boolean bookingStatus;
    private String bookingSlot;
    private Date bookingDate;
    private int advanceAmount;
    private int otp;

    public Bookings() {
    }

    public String getTurfID() {
        return turfID;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public String getCustID() {
        return custID;
    }

    public boolean isBookingStatus() {
        return bookingStatus;
    }

    public String getBookingSlot() {
        return bookingSlot;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public int getAdvanceAmount() {
        return advanceAmount;
    }

    public int getOtp() {
        return otp;
    }
}
