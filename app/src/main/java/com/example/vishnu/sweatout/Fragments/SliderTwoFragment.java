package com.example.vishnu.sweatout.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SliderTwoFragment extends Fragment {

    private static String TAG = "SLIDER TWO FRAGMENT";
    MainInterface mainInterface;
    ImageView imageView;

    public SliderTwoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_slider_two, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageView);

        List<String> images = mainInterface.getSlideImages();
        String imageurl = "";
        if(images.size()>2){
            imageurl = images.get(1);
        }
        else{
            imageurl = images.get(0);
        }
        Picasso.with(getContext()).load(imageurl).into(imageView);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }


}
