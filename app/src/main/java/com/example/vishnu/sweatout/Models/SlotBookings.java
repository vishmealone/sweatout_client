package com.example.vishnu.sweatout.Models;

public class SlotBookings {
    private int totalAmount;
    private String offerCode;
    private String custID;
    private boolean slotBookingStatus;
    private String slotID;
    private int advanceAmount;
    private int otp;

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getCustID() {
        return custID;
    }

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public boolean isSlotBookingStatus() {
        return slotBookingStatus;
    }

    public void setSlotBookingStatus(boolean slotBookingStatus) {
        this.slotBookingStatus = slotBookingStatus;
    }

    public String getSlotID() {
        return slotID;
    }

    public void setSlotID(String slotID) {
        this.slotID = slotID;
    }

    public int getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(int advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }
}
