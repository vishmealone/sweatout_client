package com.example.vishnu.sweatout.Interfaces;

import com.example.vishnu.sweatout.Models.OTPResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HttpClient {
    @GET("/API/V1/49b9925c-0e43-11e9-a895-0200cd936042/SMS/{phone_no}/AUTOGEN")
    Call<OTPResponse> SendOTP(@Path("phone_no") String phone_no);

    @GET("/API/V1/49b9925c-0e43-11e9-a895-0200cd936042/SMS/VERIFY/{session_id}/{otp}")
    Call<OTPResponse> Verifying (@Path("session_id") String session_id, @Path("otp") String otp);

    @GET("API/R1/")
    Call<OTPResponse> sendOTPwithTemplete(@Query("module") String module,
                                         @Query("apikey") String apikey,
                                         @Query("to") String to,
                                         @Query("from") String from,
                                         @Query("templatename") String templatename,
                                         @Query("var1") String var1,
                                         @Query("var2") String var2
    );
}