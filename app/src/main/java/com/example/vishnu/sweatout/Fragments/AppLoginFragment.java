package com.example.vishnu.sweatout.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vishnu.sweatout.Common.ApiClient;
import com.example.vishnu.sweatout.Interfaces.HttpClient;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.OTPResponse;
import com.example.vishnu.sweatout.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AppLoginFragment extends Fragment {

    private static String TAG = "sweatout";

    private boolean sendSMSStatus = false;
    MainInterface mainInterface;
    private SharedPreferences.Editor edit;
    private SharedPreferences sp;


    private HttpClient client;
    private Call<OTPResponse> response;

    Context context;

    private Button buttonContactSubmit;
    EditText editTextContactNumber, editTextOTPInput;
    TextView loginPageHeadText, textViewResendOTP;
    Activity activity;

    public AppLoginFragment() {
    }

    public AppLoginFragment(Activity parentActivity, Context applicationContext) {
        this.context = applicationContext;
        activity = parentActivity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_login, container, false);
        mainInterface.setNavigationHidden();

        buttonContactSubmit = (Button) view.findViewById(R.id.buttonSubmitContact);
        loginPageHeadText = (TextView) view.findViewById(R.id.loginPageHeadText);
        editTextContactNumber = (EditText) view.findViewById(R.id.editTextContactInput);
        editTextOTPInput = (EditText) view.findViewById(R.id.editTextOTPInput);
        textViewResendOTP = (TextView) view.findViewById(R.id.textViewResendOTP);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        edit = sp.edit();

        client = ApiClient.getClient().create(HttpClient.class);

        buttonContactSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (!sendSMSStatus) {
                boolean contactStatus = phoneValidator(editTextContactNumber.getText());

                if (!contactStatus) {
                    editTextContactNumber.setError("Please Enter Valid Contact!");
                } else {
                    sendOTPCode(editTextContactNumber.getText().toString());
                    mainInterface.setUserContact(editTextContactNumber.getText().toString());
                }
            } else {
                String otp = editTextOTPInput.getText().toString();
                boolean otpStatus = otpValidator(otp);

                if (!otpStatus) {
                    editTextOTPInput.setError("Please Enter Valid OTP!");
                } else {
                    sendOTPCode(editTextContactNumber.getText().toString());
                    String otpSessionID = sp.getString("otpSession", "");

                    if (!otpSessionID.equalsIgnoreCase("")) {
                        verifyOTP(otpSessionID, otp);
                    }
                }
            }
            }
        });

        textViewResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMSStatus = false;
                loginPageHeadText.setText("Register using Mobile");
                buttonContactSubmit.setText("Continue with Number");
                editTextOTPInput.setVisibility(View.INVISIBLE);
                editTextContactNumber.setVisibility(View.VISIBLE);
                edit.putString("otpSession", "");
                edit.commit();
                textViewResendOTP.setVisibility(View.INVISIBLE);
            }
        });

        return view;
    }

    private void verifyOTP(String otpSessionID, String otp) {
        response = client.Verifying(otpSessionID, otp);
        response.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
                        edit.putBoolean(getString(R.string.pref_previously_started), true);
                        edit.apply();
                        edit.commit();
                        mainInterface.openRegisterFragment();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "OTP MISMATCHED", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "OOPS SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                System.out.println(t);
            }
        });
    }


    //PHONE VALIDATOR FUNCTION
    private boolean phoneValidator(CharSequence phone) {
        if (phone != null) {
            if (phone.length() != 10) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    //PHONE VALIDATOR FUNCTION
    private boolean otpValidator(CharSequence otp) {
        if (otp != null) {
            if (otp.length() != 6) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    void sendOTPCode(final String contact) {

        if (!sendSMSStatus) {
            response = client.SendOTP(contact);
            response.enqueue(new Callback<OTPResponse>() {
                @Override
                public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                    if (response.code() == 200) {
                        Toast.makeText(getApplicationContext(), "OTP HAS BEEN SENT", Toast.LENGTH_SHORT).show();
                        sendSMSStatus = true;
                        loginPageHeadText.setText("ENTER OTP");
                        buttonContactSubmit.setText("VERIFY OTP");
                        editTextOTPInput.setVisibility(View.VISIBLE);
                        editTextContactNumber.setVisibility(View.INVISIBLE);
                        textViewResendOTP.setVisibility(View.VISIBLE);

                        edit.putString("otpSession", response.body().getDetails());
                        edit.putString("userContact", contact);
                        edit.commit();
                    } else {
                        Toast.makeText(getApplicationContext(), "OTP SENT FAILED", Toast.LENGTH_SHORT).show();
                        sendSMSStatus = false;
                    }
                }

                @Override
                public void onFailure(Call<OTPResponse> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), "OOPS SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                    sendSMSStatus = false;
                    System.out.println(t);
                }
            });
        }
    }
}
