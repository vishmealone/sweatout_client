package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Customer;
import com.example.vishnu.sweatout.R;
import com.google.gson.Gson;

import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AccountFragment extends Fragment {


    private static String TAG = "sweatout";
    MainInterface mainInterface;

    TextView userNameTextField;
    LinearLayout bookingOption,detailsOption,messageSOption,logoutOption;

    public AccountFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        bookingOption = (LinearLayout) view.findViewById(R.id.bookingOption);
        detailsOption = (LinearLayout) view.findViewById(R.id.detailsOption);
        messageSOption = (LinearLayout) view.findViewById(R.id.messageSOption);
        logoutOption = (LinearLayout) view.findViewById(R.id.logoutOption);

        userNameTextField = (TextView) view.findViewById(R.id.userNameTextField);

        Customer customerBk = mainInterface.getUserDetails();

//        Log.e(TAG,customerBk.getContact());
//        Log.e(TAG,customerBk.getEmail());
//        Log.e(TAG,customerBk.getName());
//
//        userNameTextField.setText(customerBk.getName());

        bookingOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.openTransactionsFragment();
            }
        });

        logoutOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.openLoginFragment();
            }
        });


        detailsOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.showComingSoon();
            }
        });


        messageSOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.showComingSoon();
            }
        });

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }



}
