package com.example.vishnu.sweatout.Models;

public class Offers {

    private String offerType;
    private String offerDescription;
    private int offerValue;
    private String offerID;
    private String turfID;
    private String turfSlot;

    public Offers() {}

    public String getOfferType() {
        return offerType;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public int getOfferValue() {
        return offerValue;
    }

    public String getOfferID() {
        return offerID;
    }

    public String getTurfID() {
        return turfID;
    }

    public String getTurfSlot() {
        return turfSlot;
    }
}
