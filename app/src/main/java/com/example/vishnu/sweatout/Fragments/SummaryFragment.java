package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vishnu.sweatout.Adapters.SlotAdapter;
import com.example.vishnu.sweatout.Adapters.SummarySlotAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Customer;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.Models.Turfs;
import com.example.vishnu.sweatout.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SummaryFragment extends Fragment {

    private MainInterface mainInterface;
    private TextView addressTextView, addressTextView2, addressTextView3, grossAmountValue, advanceAmount, balanceAmount,soOfferCode,offerSavingAmount;
    private static String TAG = "sweatout";
    private Button continueButton, applyOfferButton;
    private RecyclerView recyclerView;
    private Context context;
    private SummarySlotAdapter summarySlotAdapter;
    private ImageView googlemap;
    private Turfs slectedTurf;
    private LinearLayout offerApplySection,offersValueSection;
    private int grossAmount = 40;
    private int totAdvanceAmount = 500;
    private Customer cust;

    public SummaryFragment(FragmentManager supportFragmentManager, Context applicationContext) {
        context = applicationContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary, container, false);

        addressTextView = (TextView) view.findViewById(R.id.addressTextView);
        addressTextView2 = (TextView) view.findViewById(R.id.addressTextView2);
        addressTextView3 = (TextView) view.findViewById(R.id.addressTextView3);
        grossAmountValue = (TextView) view.findViewById(R.id.grossAmountValue);
        advanceAmount = (TextView) view.findViewById(R.id.advanceAmount);
        balanceAmount = (TextView) view.findViewById(R.id.balanceAmount);
        offerSavingAmount = (TextView) view.findViewById(R.id.offerSavingAmount);
        soOfferCode = (TextView) view.findViewById(R.id.soOfferCode);
        continueButton = (Button) view.findViewById(R.id.continueButton);
        applyOfferButton = (Button) view.findViewById(R.id.applyOfferButton);
        offerApplySection = (LinearLayout) view.findViewById(R.id.offerApplySection);
        offersValueSection = (LinearLayout) view.findViewById(R.id.offersValueSection);

        cust = mainInterface.getUserDetails();
        slectedTurf = mainInterface.getSelectedTurf();

        googlemap = (ImageView) view.findViewById(R.id.googlemap);


        //OPENING GOOGLE LOCATION IN GOOGLE MAP
        googlemap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Create a Uri from an intent string. Use the result to create an Intent.
                Uri gmmIntentUri = Uri.parse(slectedTurf.getTurfGLocation());
                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                // Make the Intent explicit by setting the Google Maps package
                mapIntent.setPackage("com.google.android.apps.maps");
                // Attempt to start an activity that can handle the Intent
                startActivity(mapIntent);
            }
        });

        addressTextView.setText(slectedTurf.getTurfName());
        addressTextView2.setText(slectedTurf.getTurfLocality());
        addressTextView3.setText(slectedTurf.getTurfAddress());

        Set<Map<String, String>> slots = mainInterface.getUserSelectedSlots();
        Log.e(TAG,"slots selected ");
        Log.e(TAG,""+slots);
        totAdvanceAmount = totAdvanceAmount * slots.size() + 40;

        advanceAmount.setText("Rs " + totAdvanceAmount);
        List<Map<String, String>> slotList = new ArrayList<>();
        for (Iterator<Map<String, String>> it = slots.iterator(); it.hasNext(); ) {
            Map<String, String> slot = it.next();
            grossAmount = grossAmount + Integer.parseInt(slot.get("slotCost"));
            slotList.add(slot);
        }

        grossAmountValue.setText("Rs " + grossAmount);
        recyclerView = (RecyclerView) view.findViewById(R.id.summarySlotListRecycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        int balance = grossAmount - totAdvanceAmount;
        balanceAmount.setText("Rs " + balance);

        //SETTING INITIAL RECYCLER
        setRecyclerAdapterforSlots(slotList);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            mainInterface.registerBookings();
//            mainInterface.callInstamojoPay(cust.getEmail(), cust.getContact(), totAdvanceAmount + "", "Sweat out booking advance payment", cust.getName());
            }
        });

        applyOfferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cust.isSoOfferStatus()){
                    Toast.makeText(getApplicationContext(),"SORRY YOU HAVE EXHAUSTED YOUR OFFER. KEEP TRACKING FOR MORE OFFERS",Toast.LENGTH_SHORT).show();                }
                else{
                    String offerCode = soOfferCode.getText().toString();
                    if(!offerCode.isEmpty()){
//                        Log.e(TAG,"offer code not empty");
                        if(offerCode.equalsIgnoreCase("SWEATOUT-OFFER")){
//                            Log.e(TAG,"offer code applying");
                            mainInterface.getOfferByID(offerCode);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                Log.e(TAG,"Offer fetching ");
                                Offers offer = mainInterface.getAppliedOffer();
//                                Log.e(TAG,"Offer fetching "+offer);
//                                Log.e(TAG,"Offer "+offer.getOfferDescription());
                                int reduceAmount= 0;
                                if(offer.getOfferType().equalsIgnoreCase("PERCENTAGE")){
                                    reduceAmount = (grossAmount * offer.getOfferValue())/100;
                                }
                                else{
                                    reduceAmount = grossAmount - offer.getOfferValue();
                                }
                                grossAmount = grossAmount - reduceAmount;
                                grossAmountValue.setText("Rs " + grossAmount);
                                int balance = grossAmount - totAdvanceAmount;
                                balanceAmount.setText("Rs " + balance);
                                offerApplySection.setVisibility(View.INVISIBLE);
                                offersValueSection.setVisibility(View.VISIBLE);
                                    offerSavingAmount.setText("Rs "+reduceAmount);
                                }
                            }, 2000);
                        }
                    }
                    else{
//                        Log.e(TAG,"please enter code 4");
                        Toast.makeText(getApplicationContext(),"Please enter any code apply",Toast.LENGTH_SHORT);
                    }
                }
            }
        });

        return view;
    }

    private void setRecyclerAdapterforSlots(List<Map<String, String>> slots) {
        summarySlotAdapter = new SummarySlotAdapter(recyclerView, getActivity(), slots);
        summarySlotAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(summarySlotAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

}
