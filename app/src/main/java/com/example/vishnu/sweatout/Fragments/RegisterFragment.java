package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.R;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class RegisterFragment extends Fragment {

    private static String TAG = "sweatout";

    MainInterface mainInterface;
    EditText editTextEmail, editTextName;
    Button buttonSubmit;

    public RegisterFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_register, container, false);
        editTextName = (EditText) view.findViewById(R.id.editTextName);
        editTextEmail = (EditText) view.findViewById(R.id.editTextEmail);

        buttonSubmit = (Button) view.findViewById(R.id.buttonSubmit);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean emailStatus = isValidEmail(editTextEmail.getText());
                if (emailStatus && !editTextName.getText().toString().equalsIgnoreCase("")) {
//                    Log.e(TAG, editTextEmail.getText().toString());
//                    Log.e(TAG, editTextName.getText().toString());
                    mainInterface.saveUserRegistration(editTextName.getText().toString(),editTextEmail.getText().toString());
                } else {
                    String message = "";
                    if (editTextName.getText().toString().equalsIgnoreCase("")) {
                        message = "Please do not leave your name empty";
                    } else {
                        message = "Please check mail id your have entered";
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


        return view;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }

}
