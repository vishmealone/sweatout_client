package com.example.vishnu.sweatout.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vishnu.sweatout.Adapters.CustomViewPagerAdapter;
import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Turfs;
import com.example.vishnu.sweatout.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ViewFragment extends Fragment {

    private MainInterface mainInterface;
    private static String TAG = "sweatout";
    private static final long SLIDER_TIMER = 2000; // change slider interval
    private int currentPage = 0; // this will tell us the current page available on the view pager
    // please see ViewPager Listener on the onPageSelected method to see how we are updating
    // currentPage variable

    private Button bookNowButton;
    private Turfs turf;
    private ImageView googlemap;
    private boolean isCountDownTimerActive = false; // let the timer start if and only if it has completed previous task

    private TextView playerCount, rateValue, starValue, addressTextView, addressTextView2, addressTextView3;
    private Handler handler;
    private ViewPager viewPager;

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (!isCountDownTimerActive) {
                automateSlider();
            }

            handler.postDelayed(runnable, 1000);
            // our runnable should keep running for every 1000 milliseconds (1 seconds)
        }
    };

    FragmentManager fragmentManager;

    public ViewFragment(FragmentManager supportFragmentManager) {
        // Required empty public constructor
        fragmentManager = supportFragmentManager;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view, container, false);
        mainInterface.setNavigationOpen();

        playerCount = (TextView) view.findViewById(R.id.playerCount);
        rateValue = (TextView) view.findViewById(R.id.costValue);
        starValue = (TextView) view.findViewById(R.id.starValue);
        addressTextView = (TextView) view.findViewById(R.id.addressTextView);
        addressTextView2 = (TextView) view.findViewById(R.id.addressTextView2);
        addressTextView3 = (TextView) view.findViewById(R.id.addressTextView3);

        bookNowButton = (Button) view.findViewById(R.id.bookNowButton);
        googlemap = (ImageView) view.findViewById(R.id.googlemap);

        handler = new Handler();

        handler.postDelayed(runnable, 1000);
        runnable.run();

        viewPager = (ViewPager) view.findViewById(R.id.view_pager_slider);
        CustomViewPagerAdapter viewPagerAdapter = new CustomViewPagerAdapter(fragmentManager);
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    currentPage = 0;
                } else if (position == 1) {
                    currentPage = 1;
                } else {
                    currentPage = 2;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        googlemap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Create a Uri from an intent string. Use the result to create an Intent.
                Uri gmmIntentUri = Uri.parse(turf.getTurfGLocation());
                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                // Make the Intent explicit by setting the Google Maps package
                mapIntent.setPackage("com.google.android.apps.maps");
                // Attempt to start an activity that can handle the Intent
                startActivity(mapIntent);

            }
        });

        turf = mainInterface.getSelectedTurf();

        playerCount.setText(turf.getTurfCapacity() + "");
        starValue.setText(turf.getTurfRating() + "");
        rateValue.setText(turf.getTurf_Cost() + "");
        addressTextView.setText(turf.getTurfName());
        addressTextView2.setText(turf.getTurfAddress());
        addressTextView3.setText(turf.getTurfLocality());

        bookNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterface.openTurfSlots();
            }
        });

        return view;
    }


    private void automateSlider() {
        isCountDownTimerActive = true;
        new CountDownTimer(SLIDER_TIMER, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                int nextSlider = currentPage + 1;

                if (nextSlider == 3) {
                    nextSlider = 0; // if it's last Image, let it go to the first image
                }

                viewPager.setCurrentItem(nextSlider);
                isCountDownTimerActive = false;
            }
        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        // Kill this background task once the activity has been killed
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainInterface = (MainInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainInterface = null;
    }
}
