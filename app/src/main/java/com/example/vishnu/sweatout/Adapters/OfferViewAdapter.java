package com.example.vishnu.sweatout.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vishnu.sweatout.Interfaces.MainInterface;
import com.example.vishnu.sweatout.Models.Offers;
import com.example.vishnu.sweatout.Models.Turfs;
import com.example.vishnu.sweatout.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by vishmealone on 14-11-2018.
 */

class OfferViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public OfferViewHolder(View itemView) {
        super(itemView);
//        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}

class OfferItemViewHolder extends RecyclerView.ViewHolder {

    public TextView offerID, offerDescription, offerType, turfID, slotID;
    public LinearLayout imageLayout, cardDataContainerLayout;

    public OfferItemViewHolder(View itemView) {
        super(itemView);

        turfID = (TextView) itemView.findViewById(R.id.turfID);
        slotID = (TextView) itemView.findViewById(R.id.slotID);
        offerID = (TextView) itemView.findViewById(R.id.offerID);
        offerDescription = (TextView) itemView.findViewById(R.id.offerDescription);
        offerType = (TextView) itemView.findViewById(R.id.offerType);
        cardDataContainerLayout = (LinearLayout) itemView.findViewById(R.id.cardDataContainerLayout);

    }
}

public class OfferViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    private static String TAG = "sweatout";
    MainInterface mainInterface;
    private List<Map<String, String>> turfslots = new ArrayList<>();

    boolean isLoading;
    Context context;
    List<Offers> items;
    int visibleThreshold = 5;
    int lastVisibleItem, totalItemCount;

    public OfferViewAdapter(RecyclerView recyclerView, Context context, List<Offers> items) {
        this.context = context;
        this.items = items;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.offer_recycler_item, parent, false);
            return new OfferItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.recycle_loading, parent, false);
            return new OfferItemViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OfferItemViewHolder) {

            mainInterface = (MainInterface) context;

//            Log.e(TAG,"slot position "+position);

            final Offers offer = items.get(position);
            OfferItemViewHolder viewHolder = (OfferItemViewHolder) holder;

            viewHolder.offerID.setText("Offer ID  : " + offer.getOfferID());
            viewHolder.slotID.setText("Slot ID : " + offer.getTurfSlot());

            String slotID = offer.getTurfSlot();
            List<Turfs> allRegisteredTurfs = mainInterface.getAllRegisteredTurfs();
            Log.e(TAG,slotID);
            Log.e(TAG,allRegisteredTurfs+"");

            Turfs turf = new Turfs();

            Map<String, Map<String, String>> turfSlots = new HashMap<>();

            for (int t = 0; t < allRegisteredTurfs.size(); t++) {

                if (allRegisteredTurfs.get(t).getTurfID().equalsIgnoreCase(offer.getTurfID())) {
                    turf = allRegisteredTurfs.get(t);
                    viewHolder.turfID.setText("Turf Name : " + turf.getTurfName());
                }
            }

            viewHolder.offerType.setText("  " + String.valueOf(offer.getOfferType()));
            viewHolder.offerDescription.setText("  " + String.valueOf(offer.getOfferDescription()));

            final Turfs finalTurf = turf;
            viewHolder.cardDataContainerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainInterface = (MainInterface) context;
                    if(!offer.getOfferID().equalsIgnoreCase("SWEATOUT-OFFER")) {
                        Map<String, String> userSlot = finalTurf.getSlots().get(offer.getTurfSlot());//(offer.getTurfSlot());
                        Log.e(TAG, "user slots selected");
                        Log.e(TAG, userSlot + "");
                        mainInterface.addTOSelctedSlot(userSlot);
                        Log.e(TAG, "user slots selected");
                        Log.e(TAG, userSlot + "");
                        Log.e(TAG, "turf selected");
                        Log.e(TAG, finalTurf.getTurfID() + "");
                        Log.e(TAG, finalTurf.getSlots() + "");
                        Log.e(TAG, offer.getTurfSlot() + "");

                        mainInterface.setSelectedTurf(finalTurf);
                        mainInterface.setAppliedOffer(offer);
                        mainInterface.openSummaryfragment();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "SPECIAL OFFER FOR FIRST TIME USER. Please enter code before payment", Toast.LENGTH_SHORT).show();
                    }
                }

            });

        } else if (holder instanceof OfferViewHolder) {
            OfferViewHolder loadingViewHolder = (OfferViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

}

